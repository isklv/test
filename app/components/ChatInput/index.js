import React from 'react';
import Dropzone from 'react-dropzone';

class ChatInput extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            value: ''
        }
    }

    onChange(e){

        if(this.refs.contentEditable.innerHTML){
            this.setState({value: this.refs.contentEditable.textContent});
        }
    }

    onKeyDown(e){

        const { onSend } = this.props;

        if(e.key == 'Enter'){

            if(this.state.value != ''){
                onSend(this.state.value);
                this.setState({value: ''});
                e.preventDefault();
            }
        }
    }

    onSubmit(e){
        const { onSend } = this.props;

        e.preventDefault();

        if(this.state.value){
            onSend(this.state.value);
            this.setState({value: ''});
        }

    }

    onDrop(acceptedFiles) {

        const promises = [];

        acceptedFiles.map((file, i) => {
            promises.push(new Promise((resolve, reject) => {
                const reader  = new FileReader();

                reader.onloadend = function () {
                    acceptedFiles[i] = reader.result;
                    resolve(acceptedFiles[i]);
                };

                if (file) {
                    reader.readAsDataURL(file);
                }else{
                    reject();
                }
            }));
        });

        Promise.all(promises)
            .then(results => {
                this.setState({
                    value: results[0]
                });
            });
    }

    render(){

        const { disabled } = this.props;

        return (
            <form  className={this.props.className || ''} onSubmit={this.onSubmit.bind(this)} disabled={disabled}>
              <div
                  ref="contentEditable"
                  contentEditable="true"
                  className="textarea col-xs-8"
                  name="message"
                  onKeyDown={this.onKeyDown.bind(this)}
                  onInput={this.onChange.bind(this)}
                  disabled={disabled}>
                  {this.state.value.indexOf('data:image/') != -1 ? <img src={this.state.value}/> : this.state.value}
              </div>
              <Dropzone
                  className="dropzone"
                  style={{}}
                  multiple={false}
                  maxSize={6 * 1024 * 1024}
                  onDrop={this.onDrop.bind(this)}>
                  <i className="material-icons">description</i>
              </Dropzone>
              <button className="col-xs-3" disabled={disabled}>Send</button>
            </form>
        );
    }
}

ChatInput.propTypes = {
  onSend: React.PropTypes.func,
  disabled: React.PropTypes.bool
};

export default ChatInput;
