import React from 'react';

function User(props) {

  return (
    <li className={(props.active ? 'active' : '') + ' ' + (props.messages.some(message => message.read == false) ? 'new' : '')} onClick={() => props.onSelect(props.userId)}>
        <img src={props.avatar || 'https://freeiconshop.com/wp-content/uploads/edd/person-flat.png'}/>
        <span>{props.firstName + ' ' +  props.lastName}</span>
    </li>
  );
}

User.propTypes = {
  active: React.PropTypes.bool,
  new: React.PropTypes.bool,
  userId: React.PropTypes.number,
  avatar: React.PropTypes.string,
  firstName: React.PropTypes.string,
  lastName: React.PropTypes.string,
  onSelect: React.PropTypes.func,
};

export default User;
