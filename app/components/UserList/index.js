import React from 'react';

import User from './User';

function UserList(props) {

  let content = (<li></li>);

  if (props.items) {
    content = props.items.map((item, index) => (
      <User key={`item-${index}`} {...item} onSelect={props.onSelect} />
    ));
  }

  return (
    <ul className={props.className || ''}>
        {content}
    </ul>
  );
}

UserList.propTypes = {
  items: React.PropTypes.array,
  onSelect: React.PropTypes.func,
};

export default UserList;
