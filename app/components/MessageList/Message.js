import React from 'react';

function Message(props) {

  return (
    <div className={'message-list_message ' + (props.to ? 'to' :  'from')}>
      {props.message.indexOf('data:image/') != -1 ? <img src={props.message}/> : props.message}
    </div>
  );
}

Message.propTypes = {
  to: React.PropTypes.bool,
  message: React.PropTypes.string,
};

export default Message;
