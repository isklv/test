import React from 'react';
import ReactDOM from 'react-dom';

import Message from './Message';

class MessageList extends React.Component {

    scrollToBottom() {
        const node = ReactDOM.findDOMNode(this.messagesEnd);
        node.scrollIntoView({behavior: "smooth"});
    }

    componentDidMount() {
        this.scrollToBottom();
    }

    componentDidUpdate() {
        this.scrollToBottom();
    }

    render(){
        let content = (<div></div>);

        if (this.props.items) {
          content = this.props.items.map((item, index) => (
            <Message key={`item-${index}`} to={item.to ? true : false} message={item.message} />
          ));
        }

        return (
          <div className={this.props.className || ''}>
              {content}
              <div style={ {clear: "both"} }
                ref={(el) => { this.messagesEnd = el; }}></div>
          </div>
        );
    }

}

MessageList.propTypes = {
  items: React.PropTypes.array,
};

export default MessageList;
