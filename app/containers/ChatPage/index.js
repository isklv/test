/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 */

import React from 'react';
// import Helmet from 'react-helmet';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import { makeSelectRepos, makeSelectLoading, makeSelectError } from 'containers/App/selectors';
import UserList from 'components/UserList';
import MessageList from 'components/MessageList';
import ChatInput from 'components/ChatInput';

import { initChat, loadChat, newMessage, newFakeMessage } from './actions';
import { makeSelectUsers, makeSelectMessages, makeSelectActiveChat } from './selectors';

import faker from 'faker';

export class ChatPage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  constructor(props){
      super(props);

      //fakeMessage
      setInterval(() => {this.props.newFakeMessage(); this.forceUpdate()}, 20000);
  }
  /**
   * when initial state username is not null, submit the form to load repos
   */
  componentDidMount() {
    this.props.initChat(this.fakeData());
  }

  fakeData(){
      const fakeData = [];

      for(let i = 0; i < 10; i++){

          let fakeMessages = [];
          for(let m = 0; m < 7; m++){
              fakeMessages.push({
                  to: faker.random.boolean(),
                  message: faker.lorem.paragraph(),
                  read: true
              });
          }

          fakeData.push({
              userId: faker.random.number(),
              avatar: faker.image.avatar(),
              firstName: faker.name.firstName(),
              lastName: faker.name.lastName(),
              messages: fakeMessages,
          });
      }

      return fakeData;
  }

  render() {
    const { loading, error, users, messages, selectChat, newMessage, activeChat } = this.props;
    const reposListProps = {
      loading,
      error,
    };

    return (
        <div className="chat row">
            <UserList className="col-xs-5 chat_user-list" onSelect={selectChat} items={users}/>
            <div className="row col-xs-7 bottom-xs chat_message-container">
                <MessageList className="col-xs-12 chat_message-list" items={messages}/>
            </div>
            <ChatInput className="row col-xs-offset-5 col-xs-7 chat_chat-input" onSend={newMessage} disabled={!activeChat}/>
        </div>
    );
  }
}

export function mapDispatchToProps(dispatch) {
  return {
      initChat: (data) => dispatch(initChat(data)),
      selectChat: (user) => dispatch(loadChat(user)),
      newMessage: (message) => dispatch(newMessage(message)),
      newFakeMessage: () => dispatch(newFakeMessage()),
  };
}

const mapStateToProps = createStructuredSelector({
  activeChat: makeSelectActiveChat(),
  users: makeSelectUsers(),
  messages: makeSelectMessages(),
  loading: makeSelectLoading(),
  error: makeSelectError(),
});

// Wrap the component to inject dispatch and state into it
export default connect(mapStateToProps, mapDispatchToProps)(ChatPage);
