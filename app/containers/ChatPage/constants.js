
export const INIT_CHAT = 'INIT_CHAT';
export const LOAD_CHAT = 'LOAD_CHAT';
export const NEW_MESSAGE = 'NEW_MESSAGE';
export const NEW_FAKE_MESSAGE = 'NEW_FAKE_MESSAGE';
