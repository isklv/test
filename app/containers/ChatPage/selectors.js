import { createSelector } from 'reselect';

const selectChat = (state) => state.get('chat');

const makeSelectUsers = () => createSelector(
  selectChat,
  (chatState) => Array.from(chatState.get('users'))
);

const makeSelectMessages = () => createSelector(
  selectChat,
  (chatState) => Array.from(chatState.get('messages'))
);

const makeSelectActiveChat = () => createSelector(
  selectChat,
  (chatState) => chatState.get('activeChat')
);

export {
  selectChat,
  makeSelectUsers,
  makeSelectMessages,
  makeSelectActiveChat,
};
