/*
 * ChatReducer
 *
 * The reducer takes care of our data. Using actions, we can change our
 * application state.
 * To add a new action, add it to the switch statement in the reducer function
 *
 * Example:
 * case YOUR_ACTION_CONSTANT:
 *   return state.set('yourStateVariable', true);
 */
import { fromJS } from 'immutable';
import faker from 'faker';

import {
  INIT_CHAT,
  NEW_MESSAGE,
  LOAD_CHAT,
  NEW_FAKE_MESSAGE
} from './constants';

// The initial state of the App
const initialState = fromJS({
  activeChat: null,
  users: [],
  messages: []
});

function chatReducer(state = initialState, action) {
  switch (action.type) {
    case INIT_CHAT:
      return state
        .set('users', action.users);
    case NEW_MESSAGE:
      const messages = state.get('users').filter((item) => item.userId == state.get('activeChat'))[0].messages;
      messages.push({message: action.message, to: true});
      return state
        .set('users', state.get('users').map((item) => item.userId == state.get('activeChat') ? Object.assign({}, item, {messages}) : item))
        .set('messages', messages);
    case NEW_FAKE_MESSAGE:

      const randomUser = Math.floor(Math.random() * (state.get('users').length - 0));
      const users = state.get('users');
      users[randomUser].messages.push({read: false, to: false, message: faker.lorem.paragraph()});

      return state
        .set('users', users)
        .set('messages', users[randomUser].userId == state.get('activeChat') ? users[randomUser].messages : state.get('messages'))
    case LOAD_CHAT:
      return state
        .set('activeChat', action.userId)
        .set('users',
            state.get('users')
                    .map((item) =>
                        item.userId == action.userId
                        ? Object.assign({}, item, {active: true}, {messages: item.messages.map(item => Object.assign({}, item, {read: true}))})
                        : Object.assign({}, item, {active: false})
                    )
        )
        .set('messages', state.get('users').filter((item) => item.userId == action.userId)[0].messages);
    default:
      return state;
  }
}

export default chatReducer;
