import {
  INIT_CHAT,
  LOAD_CHAT,
  NEW_MESSAGE,
  NEW_FAKE_MESSAGE,
} from './constants';

export function initChat(users) {
  return {
    type: INIT_CHAT,
    users,
    messages: [],
  };
}

export function loadChat(userId) {
  return {
    type: LOAD_CHAT,
    userId,
  };
}

export function newMessage(message) {
  return {
    type: NEW_MESSAGE,
    message,
  };
}

export function newFakeMessage() {
  return {
    type: NEW_FAKE_MESSAGE
  };
}
